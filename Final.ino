#include <WiFiEsp.h>
#include <WiFiEspClient.h>
#include <PubSubClient.h>
#include "SoftwareSerial.h"
//incluimos librería para los sensores
#include <DHT.h>
#include <MQ135.h>

//Conexión a la red wifi: nombre de la red y contraseña
#define WIFI_AP ""
#define WIFI_PASSWORD ""
// Definimos los pins para los sensores
#define LED 7
#define GP2Y A1
#define PIN_DHT11 6
#define PIN_MQ135 A0

//Nombre o IP del servidor mosquitto
char server[50] = "";

//Inicializamos el objeto de cliente esp
WiFiEspClient espClient;

//Iniciamos el objeto subscriptor del cliente 
//con el objeto del cliente
PubSubClient client(espClient);

//Conexión serial para el esp con una comunicación
//serial, pines 2: rx y 3: tx
SoftwareSerial soft(2, 3);

//Contador para el envio de datos
unsigned long lastSend;

int status = WL_IDLE_STATUS;

//inicializamos el sensor GP2Y
unsigned int samplingTime = 280;
unsigned int deltaTime = 40;
unsigned int sleepTime = 9680;
float calcVoltage = 0;
float dustDensity = 0;

//inicializamos el sensor DHT11
DHT dht(PIN_DHT11, DHT11);
float humidity = 0;
float temperature = 0;

//inicializamos el sensor MQ135
MQ135 mq135_sensor(PIN_MQ135);
int co2lvl = 0;
 
void setup() {
    //pins salida para led
    pinMode(LED, OUTPUT);
    //Inicializamos la comunicación serial para el log
    Serial.begin(9600);
    //Iniciamos la conexión a la red WiFi
    InitWiFi();
    //Colocamos la referencia del servidor y el puerto
    client.setServer( server, 1883 );
    lastSend = 0;
    dht.begin();
}

void loop() {
    //Validamos si el modulo WiFi aun esta conectado a la red
    status = WiFi.status();
    if(status != WL_CONNECTED) {
        //Si falla la conexión, reconectamos el modulo
        reconnectWifi();
    }

    //Validamos si esta la conexión del servidor
    if(!client.connected() ) {
        //Si falla reintentamos la conexión
        reconnectClient();
    }

    //Creamos un contador para enviar la data cada 2 segundos
    if(millis() - lastSend > 2000 ) {
        sendDataTopic();
        lastSend = millis();
    }

    client.loop();
}

void sendDataTopic()
{
    // Prepare a JSON payload string
    getData();
    String json = buildJson();
    char jsonStr[200];
    json.toCharArray (jsonStr, 200);
    Serial.print(jsonStr);
    delay(5000);    

    client.publish( "casa/piso-1/sala/nodo-iot", jsonStr );
}

//Inicializamos la conexión a la red wifi
void InitWiFi()
{
    //Inicializamos el puerto serial
    soft.begin(9600);
    //Iniciamos la conexión wifi
    WiFi.init(&soft);
    //Verificamos si se pudo realizar la conexión al wifi
    //si obtenemos un error, lo mostramos por log y denememos el programa
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("El modulo WiFi no esta presente");
        while (true);
    }
    reconnectWifi();
}

void reconnectWifi() {
    Serial.println("Iniciar conexión a la red WIFI");
    while(status != WL_CONNECTED) {
        Serial.print("Intentando conectarse a WPA SSID: ");
        Serial.println(WIFI_AP);
        //Conectar a red WPA/WPA2
        status = WiFi.begin(WIFI_AP, WIFI_PASSWORD);
        delay(500);
    }
    Serial.println("Conectado a la red WIFI");
}

void reconnectClient() {
    //Creamos un loop en donde intentamos hacer la conexión
    while(!client.connected()) {
        Serial.print("Conectando a: ");
        Serial.println(server);
        //Creamos una nueva cadena de conexión para el servidor
        //e intentamos realizar la conexión nueva
        //si requiere usuario y contraseña la enviamos connect(clientId, username, password)
        String clientId = "ESP8266Client-" + String(random(0xffff), HEX);
        if(client.connect(clientId.c_str())) {
            Serial.println("[DONE]");
        } else {
            Serial.print( "[FAILED] [ rc = " );
            Serial.print( client.state() );
            Serial.println( " : retrying in 5 seconds]" );
            delay( 5000 );
        }
    }
}

// Function to get data from sensors
void getData() {
  // GP2Y
  digitalWrite(LED,LOW); // power on the LED
  delayMicroseconds(samplingTime);
  float gp2YInput = analogRead(GP2Y); // read the dust value
  delayMicroseconds(deltaTime);
  digitalWrite(LED,HIGH); // turn the LED off
  delayMicroseconds(sleepTime);
  // 0 - 5V mapped to 0 - 1023 integer values
  calcVoltage = gp2YInput * ( 5.0 / 1024.0 );
  dustDensity = 17 * calcVoltage - 0.1;
  Serial.print("Dust Sensor ");
  Serial.print(dustDensity); // unit: ug/m3
  Serial.print(F(" ug/m3 "));
  delay(1000);

  //DHT11
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  Serial.print("Humedad: ");
  Serial.print(humidity);
  Serial.print(" % - ");
  Serial.print("Temperatura: ");
  Serial.print(temperature);
  Serial.print(" C ");
  delay(1000);

  //MQ135
  float co2Input = analogRead(PIN_MQ135);
  co2lvl = co2Input - 115;
  co2lvl = map(co2lvl, 0, 1024, 400, 5000);
  Serial.print(" co2lvl: ");
  Serial.print(co2lvl);
  Serial.println(" ppm ");
  delay(1000);  
}

// Function to build a Json
String buildJson() {
  String data = "";
  data+= (float)temperature;
  data+= ",";
  data+=(float)humidity;
  data+= ",";
  data+=(int)co2lvl;
  data+= ",";
  data+=(float)dustDensity;
  return data;
}
